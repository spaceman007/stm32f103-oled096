# stm32f103-oled096

#### 介绍

芯片：stm32f103c8t6

屏幕：0.96 寸 12864 oled 屏，屏幕驱动为 ssd1315

操作系统：Linux

#### 编译
执行 `make` 命令

#### 烧写
连接 st-link

执行如下命令：

```shell
st-flash write ./build/stm32f1.bin 0x8000000
```